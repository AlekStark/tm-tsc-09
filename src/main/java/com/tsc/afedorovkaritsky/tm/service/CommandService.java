package com.tsc.afedorovkaritsky.tm.service;

import com.tsc.afedorovkaritsky.tm.api.ICommandRepository;
import com.tsc.afedorovkaritsky.tm.api.ICommandService;
import com.tsc.afedorovkaritsky.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
