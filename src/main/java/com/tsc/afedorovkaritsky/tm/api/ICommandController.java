package com.tsc.afedorovkaritsky.tm.api;

public interface ICommandController {
    void showAbout();

    void exit();

    void showArguments();

    void showCommands();

    void showErrorArgument();

    void showErrorCommand();

    void showHelp();

    void showInfo();

    void showVersion();
}
